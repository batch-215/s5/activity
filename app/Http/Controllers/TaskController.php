<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $tasks=Task::all();
        return view('task',compact('tasks'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validation
        $this->validate($request, [
            'task' => 'required',
            'status' => 'required'
        ]);

        //Create Task
        $task = new Task(); //one row in your table
        $task->name = $request->input('task');
        $task->status = $request->input('status');
        //$task->user_id = auth()->user()->id;
        $task->save();

        return redirect('/home')->with('success', 'Task Created');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $task=Task::where('id',$id)->first();
        return view('edit-task',compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('tasks.edit',['task'=>$id])->withErrors($validator);
        }



        $task=Task::where('id',$id)->first();
        $task->title=$request->get('title');
        $task->is_completed=$request->get('is_completed');
        $task->save();

        return redirect()->route('tasks.index')->with('success', 'Updated Task');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        Task::where('id',$id)->delete();
        return redirect()->route('taks.index')->with('success', 'Deleted Task');
    }
}


